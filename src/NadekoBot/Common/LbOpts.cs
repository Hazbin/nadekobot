﻿using CommandLine;

namespace NadekoBot.Common
{
    public class LbOpts : INadekoCommandOptions
    {
        [Option('f', "full", Default = false, HelpText = "Include users who are no longer on the server.")]
        public bool Full { get; set; }
        public void NormalizeOptions()
        {

        }
    }
}
